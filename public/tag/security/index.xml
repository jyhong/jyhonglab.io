<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Security | Junyuan Hong</title>
    <link>https://jyhong.gitlab.io/tag/security/</link>
      <atom:link href="https://jyhong.gitlab.io/tag/security/index.xml" rel="self" type="application/rss+xml" />
    <description>Security</description>
    <generator>Wowchemy (https://wowchemy.com)</generator><language>en-us</language><copyright>© 2025 Junyuan Hong</copyright><lastBuildDate>Sat, 06 Jan 2024 13:08:20 +0800</lastBuildDate>
    <image>
      <url>https://jyhong.gitlab.io/media/icon_hu27b6e1f5ea1ee5d309bdcac14a7db538_316_512x512_fill_lanczos_center_2.png</url>
      <title>Security</title>
      <link>https://jyhong.gitlab.io/tag/security/</link>
    </image>
    
    <item>
      <title>Safe and Robust Watermark Injection with a Single OoD Image</title>
      <link>https://jyhong.gitlab.io/publication/2023one_image_watermark/</link>
      <pubDate>Sat, 06 Jan 2024 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/2023one_image_watermark/</guid>
      <description></description>
    </item>
    
    <item>
      <title>Revisiting Data-Free Knowledge Distillation with Poisoned Teachers</title>
      <link>https://jyhong.gitlab.io/publication/datafree_backdoor2023icml/</link>
      <pubDate>Tue, 25 Apr 2023 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/datafree_backdoor2023icml/</guid>
      <description>&lt;p&gt;To tailor the highly performant large models for the budget-constrained devices, knowledge distillation (KD) and more recently data-free KD, has emerged as a fundamental tool in the DL community.
Data-free KD, in particular, can transfer knowledge from a pre-trained large model (known as the &lt;em&gt;teacher model&lt;/em&gt;) to a smaller model (known as the &lt;em&gt;student model&lt;/em&gt;) without access to the original training data of the teacher model. The non-requirement of training data generalizes KD to broad real-world scenarios, where data access is restricted for privacy and security concerns.
For instance, many countries have strict laws on accessing facial images, financial records, and medical information.&lt;/p&gt;
&lt;p&gt;Despite the benefits of data-free KD and the vital role it has been playing, a major security concern has been overlooked in its development and implementation: &lt;em&gt;Can a student trust the knowledge transferred from an untrusted teacher?&lt;/em&gt;
The untrustworthiness comes from the non-trivial chance that pre-trained models could be retrieved from non-sanitized or unverifiable sources, for example, third-party model vendors or malicious clients in federated learning.
One significant risk is from the &lt;em&gt;backdoor&lt;/em&gt; pre-implanted into a teacher model, which alters model behaviors drastically in the presence of predesigned triggers but remains silent on clean samples.
As traditional attacks typically require to poison training data, it remains unclear if student models distilled from a poisoned teacher will suffer from the same threat without using the poisoned data.&lt;/p&gt;
&lt;figure&gt;
&lt;img src=&#34;data-free_results_acc.png&#34; width=100% title=&#34;Backdoor attacks&#34;&gt;
&lt;figcaption&gt;
  Fig 1: Backdoor Attack Success Rates (&lt;b&gt;ASRs&lt;/b&gt;) of the distilled student model using the vanilla KD with clean in-distribution samples (a) and data-free KD using synthetic (b, c) or OOD (d) samples. The clean accuracy (&lt;b&gt;Acc&lt;/b&gt;) of each figure is plotted with standard deviations among different attack-poisoned CIFAR-10. We run each KD method with different but sufficient training epochs to ensure convergence. Existing data-free KD methods may lead to the transfer of backdoor knowledge when poisoned teachers&#39; participation.
  &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;figure&gt;
&lt;img src=&#34;all_trigger.png&#34; width=85% title=&#34;Backdoor triggers&#34;&gt;
&lt;figcaption&gt;
  Fig 2: Trigger visualization and teacher model performances on CIFAR-10. The performance (&lt;b&gt;ASR/Acc&lt;/b&gt;) of the poisoned teacher using each backdoor attack is provided beneath each trigger&#39;s name. We envision the backdoored example for each attack on CIFAR-10.
  &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;In this paper, we take the first leap to uncover the &lt;em&gt;data-free backdoor transfer&lt;/em&gt; from a poisoned teacher to a student through comprehensive experiments on 10 backdoor attacks.
We evaluated one vanilla KD using clean training data and three training-data-free KD method which use synthetic data (ZSKT&lt;sup id=&#34;fnref:1&#34;&gt;&lt;a href=&#34;#fn:1&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;1&lt;/a&gt;&lt;/sup&gt; &amp;amp; CMI &lt;sup id=&#34;fnref:2&#34;&gt;&lt;a href=&#34;#fn:2&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;2&lt;/a&gt;&lt;/sup&gt;) or out-of-distribution (OOD) data as surrogate distillation data&lt;sup id=&#34;fnref:3&#34;&gt;&lt;a href=&#34;#fn:3&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;3&lt;/a&gt;&lt;/sup&gt;.&lt;/p&gt;
&lt;p&gt;Our main observations are summarized as follows and essentially imply two identified risks in data-free KD.&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;Vanilla KD does not transfer backdoors by using clean in-distribution data, while all three training-data-free distillations suffer from backdoor transfer by 3 to 8 types of triggers out of 10 with a more than 90% attack success rate. Contradicting the two results indicates the &lt;strong&gt;poisonous nature of the surrogate distillation&lt;/strong&gt; data in data-free KD.&lt;/li&gt;
&lt;li&gt;The successful attack on distillation using trigger-free out-of-distribution (OOD) data demonstrate that triggers are not essential for backdoor injection, but the &lt;strong&gt;poisoned teacher supervision&lt;/strong&gt; is.&lt;/li&gt;
&lt;/ol&gt;
&lt;figure&gt;
&lt;img src=&#34;ABD_benchmark.png&#34; width=75% title=&#34;Benchmark&#34;&gt;
&lt;figcaption&gt;
  Fig 3: ABD is effective in different data-free distillation methods on CIFAR-10 with WRN16-2 (Teacher) and WRN16-1 (student). 
  &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;Upon observing the two identified risks, we propose a plug-in defensive method, Anti-Backdoor Data-Free KD (&lt;strong&gt;ABD&lt;/strong&gt;), that works with general data-free KD frameworks. ABD aims to suppress and remove any backdoor knowledge being transferred to the student, thus mitigating the impact of backdoors. The high-level idea of ABD is two-fold:
&lt;strong&gt;(SV) Shuffling Vaccine&lt;/strong&gt; during distillation:~suppress samples containing potential backdoor knowledge being fed to the teacher (mitigating backdoor information participates in the KD); Student
&lt;strong&gt;(SR) Self-Retrospection&lt;/strong&gt; after distillation:~ synthesize potential learned backdoor knowledge and unlearns them at later training epochs (the backstop to unlearn acquired malicious knowledge).
ABD is effective on defending various backdoor attacks with different patterns and is a plug-in defense that can be used seamlessly with all three types of data-free KD.&lt;/p&gt;
&lt;section class=&#34;footnotes&#34; role=&#34;doc-endnotes&#34;&gt;
&lt;hr&gt;
&lt;ol&gt;
&lt;li id=&#34;fn:1&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Micaelli, P., &amp;amp; Storkey, A. J. (2019). Zero-shot knowledge transfer via adversarial belief matching. NeurIPS. &lt;a href=&#34;#fnref:1&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;li id=&#34;fn:2&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Fang, G., Song, J., Wang, X., Shen, C., Wang, X., &amp;amp; Song, M. (2021). Contrastive model inversion for data-free knowledge distillation. IJCAI. &lt;a href=&#34;#fnref:2&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;li id=&#34;fn:3&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Asano, Y. M., &amp;amp; Saeed, A. (2023). Extrapolating from a single image to a thousand classes using distillation. ICLR. &lt;a href=&#34;#fnref:3&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;/ol&gt;
&lt;/section&gt;
</description>
    </item>
    
    <item>
      <title>How Robust is Your Fairness? Evaluating and Sustaining Fairness under Unseen Distribution Shifts</title>
      <link>https://jyhong.gitlab.io/publication/fair-robust2023tmlr/</link>
      <pubDate>Sun, 19 Feb 2023 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/fair-robust2023tmlr/</guid>
      <description></description>
    </item>
    
    <item>
      <title>Federated Robustness Propagation: Sharing Adversarial Robustness in Federated Learning</title>
      <link>https://jyhong.gitlab.io/publication/frp2023/</link>
      <pubDate>Mon, 02 Jan 2023 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/frp2023/</guid>
      <description></description>
    </item>
    
    <item>
      <title>Holistic Trustworthy ML</title>
      <link>https://jyhong.gitlab.io/project/holistic-trustworthy/</link>
      <pubDate>Tue, 27 Sep 2022 00:00:00 +0000</pubDate>
      <guid>https://jyhong.gitlab.io/project/holistic-trustworthy/</guid>
      <description>&lt;p&gt;In the era of deep learning and facing the simultaneously-induced tremendous risks, my vision is to &lt;strong&gt;enhance the trustworthiness of machine learning&lt;/strong&gt;.
&lt;em&gt;Fairness, robustness, security, inclusiveness, and privacy&lt;/em&gt; are the core targets within the scope of trustworthiness.
For example, recognizing objects by self-driving cars requires the model to be fair regardless of the execution countries, robust in different environments, secure against implicit backdoors, inclusive to heterogeneous computation/data nodes, and preserve the privacy of sensitive training data.
Recently, attaining trustworthiness has become a fundamental requirement for machine learning to be reliably used in human-centered activities.&lt;/p&gt;
&lt;h2 id=&#34;privacy-centric-trustworthy-learning&#34;&gt;Privacy-Centric Trustworthy Learning&lt;/h2&gt;
&lt;p&gt;My recent research focuses on the trustworthiness of machine learning within the privacy-preserving learning frameworks and I outline my work as the &lt;strong&gt;Privacy-Centric Trustworthy Learning&lt;/strong&gt;.
As learning large models from private data has been an essential strategy facing the increasing demand for massive data, for example, 45TB text data for training the language model (GPT-3), protecting data privacy has become the prerequisite before pursuing the fairness, robustness, and security of models.
However, traditional trustworthy machine learning is typically single-dimensional, for example, considering fairness only without privacy.
As outlined below, my research fills the gap by developing &lt;em&gt;trustworthiness-aware algorithms and models within the privacy-preserving data and computation frameworks&lt;/em&gt;, for example, federated learning.
In federated learning or other similarly-principled frameworks, data are excluded from communication between different data sources and training is executed on local devices for each user.&lt;/p&gt;
&lt;p&gt;
&lt;img src=&#34;privacy_x.png&#34; alt=&#34;&#34;&gt;
&lt;/p&gt;
&lt;p&gt;Such frameworks pose interwoven and non-trivial challenges in terms of invisible risks of trustworthiness and increased computation loads to local devices.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;(1) Invisible risks by invisible data.&lt;/strong&gt;
As the raw data are invisible to other users in federated learning, the biased and potential poison samples are not visible to the global system, either.
Therefore, defending against such biases or noise will become harder compared to that in a centralized setting.
We unraveled that such data invisibility may result in the transfer of poison knowledge implicitly yielding &lt;strong&gt;insecure&lt;/strong&gt; models, in data-free distillation &lt;a href=&#34;publication/datafree_backdoor2023icml/&#34;&gt;[ICML23]&lt;/a&gt;, which was used for federated learning &lt;a href=&#34;publication/data_free_fl/&#34;&gt;[ICML21]&lt;/a&gt;.
When clients&#39; data are mutually unaware of each other, we demonstrate that the bias between users may be ignored and results in &lt;strong&gt;unfairness&lt;/strong&gt; &lt;a href=&#34;publication/fade2021kdd/&#34;&gt;[KDD21]&lt;/a&gt;.
For both the security and fairness challenges, we proposed corresponding countermeasures by adversarial learning strategies.&lt;/p&gt;
&lt;p&gt;&lt;strong&gt;(2) Low inclusiveness by increased computation costs for trustworthiness.&lt;/strong&gt;
The existing computation barrier of trustworthy machine learning makes the trustworthiness and learning no longer inclusive or accessible to many users in federated learning that requires on-device training.
For example, to achieve robustness, extra computation has been devoted to adversarial training or out-of-distribution (OoD) detection.
The overhead limits low-resource users to gain robustness because of the high cost of robust training in terms of data or computation.
We provided the first solutions to sharing &lt;strong&gt;adversarial robustness&lt;/strong&gt; &lt;a href=&#34;publication/frp2023/&#34;&gt;[AAAI23]&lt;/a&gt; and &lt;strong&gt;OoD robustness&lt;/strong&gt; &lt;a href=&#34;publication/foster2023/&#34;&gt;[ICLR23]&lt;/a&gt; by leveraging collaborative computation and communication.
Except for robustness, low-resource users are often excluded from the federated learning to train a large model.
We proposed algorithms to make the training &lt;strong&gt;inclusively affordable&lt;/strong&gt; for different devices, where models are for the first time customizable both in training and test time &lt;a href=&#34;publication/split_mix/&#34;&gt;[ICLR22]&lt;/a&gt;.
In addition, extremely low-resource devices, for instance, the Internet-of-Thing devices, are not suitable for training by design on memory and coding systems.
Thus, we provide the first sampling-based framework &lt;a href=&#34;publication/ecos/&#34;&gt;[NeurIPS22]&lt;/a&gt; to &lt;strong&gt;inclusively accommodate&lt;/strong&gt; the low-resource users.&lt;/p&gt;
</description>
    </item>
    
    <item>
      <title>Trap and Replace: Defending Backdoor Attacks by Trapping Them into an Easy-to-Replace Subnetwork</title>
      <link>https://jyhong.gitlab.io/publication/trap_backdoor/</link>
      <pubDate>Thu, 22 Sep 2022 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/trap_backdoor/</guid>
      <description></description>
    </item>
    
  </channel>
</rss>
