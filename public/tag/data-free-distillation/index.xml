<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Data-free Distillation | Junyuan Hong</title>
    <link>https://jyhong.gitlab.io/tag/data-free-distillation/</link>
      <atom:link href="https://jyhong.gitlab.io/tag/data-free-distillation/index.xml" rel="self" type="application/rss+xml" />
    <description>Data-free Distillation</description>
    <generator>Wowchemy (https://wowchemy.com)</generator><language>en-us</language><copyright>© 2025 Junyuan Hong</copyright><lastBuildDate>Sat, 06 Jan 2024 13:08:20 +0800</lastBuildDate>
    <image>
      <url>https://jyhong.gitlab.io/media/icon_hu27b6e1f5ea1ee5d309bdcac14a7db538_316_512x512_fill_lanczos_center_2.png</url>
      <title>Data-free Distillation</title>
      <link>https://jyhong.gitlab.io/tag/data-free-distillation/</link>
    </image>
    
    <item>
      <title>Safe and Robust Watermark Injection with a Single OoD Image</title>
      <link>https://jyhong.gitlab.io/publication/2023one_image_watermark/</link>
      <pubDate>Sat, 06 Jan 2024 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/2023one_image_watermark/</guid>
      <description></description>
    </item>
    
    <item>
      <title>Revisiting Data-Free Knowledge Distillation with Poisoned Teachers</title>
      <link>https://jyhong.gitlab.io/publication/datafree_backdoor2023icml/</link>
      <pubDate>Tue, 25 Apr 2023 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/datafree_backdoor2023icml/</guid>
      <description>&lt;p&gt;To tailor the highly performant large models for the budget-constrained devices, knowledge distillation (KD) and more recently data-free KD, has emerged as a fundamental tool in the DL community.
Data-free KD, in particular, can transfer knowledge from a pre-trained large model (known as the &lt;em&gt;teacher model&lt;/em&gt;) to a smaller model (known as the &lt;em&gt;student model&lt;/em&gt;) without access to the original training data of the teacher model. The non-requirement of training data generalizes KD to broad real-world scenarios, where data access is restricted for privacy and security concerns.
For instance, many countries have strict laws on accessing facial images, financial records, and medical information.&lt;/p&gt;
&lt;p&gt;Despite the benefits of data-free KD and the vital role it has been playing, a major security concern has been overlooked in its development and implementation: &lt;em&gt;Can a student trust the knowledge transferred from an untrusted teacher?&lt;/em&gt;
The untrustworthiness comes from the non-trivial chance that pre-trained models could be retrieved from non-sanitized or unverifiable sources, for example, third-party model vendors or malicious clients in federated learning.
One significant risk is from the &lt;em&gt;backdoor&lt;/em&gt; pre-implanted into a teacher model, which alters model behaviors drastically in the presence of predesigned triggers but remains silent on clean samples.
As traditional attacks typically require to poison training data, it remains unclear if student models distilled from a poisoned teacher will suffer from the same threat without using the poisoned data.&lt;/p&gt;
&lt;figure&gt;
&lt;img src=&#34;data-free_results_acc.png&#34; width=100% title=&#34;Backdoor attacks&#34;&gt;
&lt;figcaption&gt;
  Fig 1: Backdoor Attack Success Rates (&lt;b&gt;ASRs&lt;/b&gt;) of the distilled student model using the vanilla KD with clean in-distribution samples (a) and data-free KD using synthetic (b, c) or OOD (d) samples. The clean accuracy (&lt;b&gt;Acc&lt;/b&gt;) of each figure is plotted with standard deviations among different attack-poisoned CIFAR-10. We run each KD method with different but sufficient training epochs to ensure convergence. Existing data-free KD methods may lead to the transfer of backdoor knowledge when poisoned teachers&#39; participation.
  &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;figure&gt;
&lt;img src=&#34;all_trigger.png&#34; width=85% title=&#34;Backdoor triggers&#34;&gt;
&lt;figcaption&gt;
  Fig 2: Trigger visualization and teacher model performances on CIFAR-10. The performance (&lt;b&gt;ASR/Acc&lt;/b&gt;) of the poisoned teacher using each backdoor attack is provided beneath each trigger&#39;s name. We envision the backdoored example for each attack on CIFAR-10.
  &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;In this paper, we take the first leap to uncover the &lt;em&gt;data-free backdoor transfer&lt;/em&gt; from a poisoned teacher to a student through comprehensive experiments on 10 backdoor attacks.
We evaluated one vanilla KD using clean training data and three training-data-free KD method which use synthetic data (ZSKT&lt;sup id=&#34;fnref:1&#34;&gt;&lt;a href=&#34;#fn:1&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;1&lt;/a&gt;&lt;/sup&gt; &amp;amp; CMI &lt;sup id=&#34;fnref:2&#34;&gt;&lt;a href=&#34;#fn:2&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;2&lt;/a&gt;&lt;/sup&gt;) or out-of-distribution (OOD) data as surrogate distillation data&lt;sup id=&#34;fnref:3&#34;&gt;&lt;a href=&#34;#fn:3&#34; class=&#34;footnote-ref&#34; role=&#34;doc-noteref&#34;&gt;3&lt;/a&gt;&lt;/sup&gt;.&lt;/p&gt;
&lt;p&gt;Our main observations are summarized as follows and essentially imply two identified risks in data-free KD.&lt;/p&gt;
&lt;ol&gt;
&lt;li&gt;Vanilla KD does not transfer backdoors by using clean in-distribution data, while all three training-data-free distillations suffer from backdoor transfer by 3 to 8 types of triggers out of 10 with a more than 90% attack success rate. Contradicting the two results indicates the &lt;strong&gt;poisonous nature of the surrogate distillation&lt;/strong&gt; data in data-free KD.&lt;/li&gt;
&lt;li&gt;The successful attack on distillation using trigger-free out-of-distribution (OOD) data demonstrate that triggers are not essential for backdoor injection, but the &lt;strong&gt;poisoned teacher supervision&lt;/strong&gt; is.&lt;/li&gt;
&lt;/ol&gt;
&lt;figure&gt;
&lt;img src=&#34;ABD_benchmark.png&#34; width=75% title=&#34;Benchmark&#34;&gt;
&lt;figcaption&gt;
  Fig 3: ABD is effective in different data-free distillation methods on CIFAR-10 with WRN16-2 (Teacher) and WRN16-1 (student). 
  &lt;/figcaption&gt;
&lt;/figure&gt;
&lt;p&gt;Upon observing the two identified risks, we propose a plug-in defensive method, Anti-Backdoor Data-Free KD (&lt;strong&gt;ABD&lt;/strong&gt;), that works with general data-free KD frameworks. ABD aims to suppress and remove any backdoor knowledge being transferred to the student, thus mitigating the impact of backdoors. The high-level idea of ABD is two-fold:
&lt;strong&gt;(SV) Shuffling Vaccine&lt;/strong&gt; during distillation:~suppress samples containing potential backdoor knowledge being fed to the teacher (mitigating backdoor information participates in the KD); Student
&lt;strong&gt;(SR) Self-Retrospection&lt;/strong&gt; after distillation:~ synthesize potential learned backdoor knowledge and unlearns them at later training epochs (the backstop to unlearn acquired malicious knowledge).
ABD is effective on defending various backdoor attacks with different patterns and is a plug-in defense that can be used seamlessly with all three types of data-free KD.&lt;/p&gt;
&lt;section class=&#34;footnotes&#34; role=&#34;doc-endnotes&#34;&gt;
&lt;hr&gt;
&lt;ol&gt;
&lt;li id=&#34;fn:1&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Micaelli, P., &amp;amp; Storkey, A. J. (2019). Zero-shot knowledge transfer via adversarial belief matching. NeurIPS. &lt;a href=&#34;#fnref:1&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;li id=&#34;fn:2&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Fang, G., Song, J., Wang, X., Shen, C., Wang, X., &amp;amp; Song, M. (2021). Contrastive model inversion for data-free knowledge distillation. IJCAI. &lt;a href=&#34;#fnref:2&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;li id=&#34;fn:3&#34; role=&#34;doc-endnote&#34;&gt;
&lt;p&gt;Asano, Y. M., &amp;amp; Saeed, A. (2023). Extrapolating from a single image to a thousand classes using distillation. ICLR. &lt;a href=&#34;#fnref:3&#34; class=&#34;footnote-backref&#34; role=&#34;doc-backlink&#34;&gt;&amp;#x21a9;&amp;#xfe0e;&lt;/a&gt;&lt;/p&gt;
&lt;/li&gt;
&lt;/ol&gt;
&lt;/section&gt;
</description>
    </item>
    
  </channel>
</rss>
