<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Dynamic Allocation | Junyuan Hong</title>
    <link>https://jyhong.gitlab.io/tag/dynamic-allocation/</link>
      <atom:link href="https://jyhong.gitlab.io/tag/dynamic-allocation/index.xml" rel="self" type="application/rss+xml" />
    <description>Dynamic Allocation</description>
    <generator>Wowchemy (https://wowchemy.com)</generator><language>en-us</language><copyright>© 2025 Junyuan Hong</copyright><lastBuildDate>Thu, 07 Apr 2022 13:08:20 +0800</lastBuildDate>
    <image>
      <url>https://jyhong.gitlab.io/media/icon_hu27b6e1f5ea1ee5d309bdcac14a7db538_316_512x512_fill_lanczos_center_2.png</url>
      <title>Dynamic Allocation</title>
      <link>https://jyhong.gitlab.io/tag/dynamic-allocation/</link>
    </image>
    
    <item>
      <title>Dynamic Privacy Budget Allocation Improves Data Efficiency of Differentially Private Gradient Descent</title>
      <link>https://jyhong.gitlab.io/publication/ondynamic/</link>
      <pubDate>Thu, 07 Apr 2022 13:08:20 +0800</pubDate>
      <guid>https://jyhong.gitlab.io/publication/ondynamic/</guid>
      <description>&lt;p&gt;Utility upper bounds are a critical metric for privacy schedules, which characterizes the maximum utility
that a schedule can deliver in theory. Wang et al. [34] is the first
to prove the utility bound under the PL condition. Recently, Zhou
et al. proved the utility bound by using the momentum of gradients [17, 25]. In this paper, we improve the upper bound by a
more accurate estimation of the dynamic influence of step noise.
We show that introducing a dynamic schedule further boosts the
sample-efficiency of the upper bound. Table 1 summarizes the upper bounds of a selection of state-of-the-art algorithms based on private gradients (up block, see Appendix B for the full list), and
methods studied in this paper (down block), showing the benefits
of dynamic influence.&lt;/p&gt;
&lt;p&gt;Especially, a closely-related work by Feldman et al. achieved
a convergence rate similar to ours in terms of generalization error bounds (c.f. SSGD in Table 2), by dynamically adjusting batch
sizes [11]. However, the approach requires controllable batch sizes,
which may not be feasible in many applications. In federated learning, for example, where users update models locally and then pass
the parameters to server for aggregation, the server has no control
over batch sizes, and coordinating users to use varying batch sizes
may not be realistic. On the other hand, our proposed method can
still be applied for enhancing utility, as the server can dynamically
allocate privacy budget for each round when the presence of a user
in the global aggregation is privatized [21].&lt;/p&gt;
&lt;p&gt;
&lt;img src=&#34;bd_comp.png&#34; alt=&#34;&#34;&gt;
&lt;/p&gt;
&lt;p&gt;In brief, given a sharper loss function, the dynamic budget allocation allows the DPSGD to run for more private iterations and results in lower excess expected risks.&lt;/p&gt;
&lt;p&gt;
&lt;img src=&#34;T.png&#34; alt=&#34;&#34;&gt;
&lt;/p&gt;
&lt;p&gt;
&lt;img src=&#34;EER.png&#34; alt=&#34;&#34;&gt;
&lt;/p&gt;
</description>
    </item>
    
  </channel>
</rss>
